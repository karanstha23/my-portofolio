<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_histories', function (Blueprint $table) {
            $table->id();

            $table->string('job_title');
            $table->string('organisation_name');
            $table->string('organisation_location');
            $table->date('starting_date');
            $table->date('ending_date')->nullable();
            $table->enum('current_job',[0,1])->default(0);
            $table->text('roles_responsibilities');
            $table->SoftDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_histories');
    }
}
