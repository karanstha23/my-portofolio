<div class="my-recent-works py-5">
    <div class="container pt-5">
        <div class="h1">My Recent Works</div>
        <div class="bar"></div>
    </div>
    <div class="container my-recent-works-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12 py-5 text-center">
                <h1 class="display-3">I love what I do, check out some of my latest works</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6 col-6 text-center recent-work-lists">
                <img src="images/portofolio.png" alt="" class="" style="display: block; width: 100%; height: 100%">
                <div class="text py-2">
                    <h2>My Portofolio</h2>
                    <p>Learn More</p>
                </div>
            </div>
            {{-- <div class="col-lg-6 col-md-12 col-12 py-5 text-center">
                <img src="images/portofolio.png" alt="" class="img-fluid" height="500px">

            </div> --}}
        </div>

    </div>
</div>
