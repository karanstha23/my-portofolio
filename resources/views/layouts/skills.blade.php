<div class="my-skills py-5">
    <div class="container pt-5">
        <div class="h1">Professional Skils</div>
        <div class="bar"></div>
    </div>
    <div class="container my-skills-container">
        <div class="row">
            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fab fa-laravel fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            php(Laravel) Developer
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fas fa-laptop-code fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            Web Development and Optimisation
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fab fa-chrome fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            UI Design
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fas fa-phone-alt fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            IT Support
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fas fa-network-wired fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            API Design
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fas fa-mobile fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            Mobile App Creation
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fab fa-html5 fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            HTML/ CSS
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fab fa-bootstrap fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            Bootstrap
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fab fa-android fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            Flutter
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fab fa-git fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            Version Control (Gitlab, Bitbucket)
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 col-6 py-5 text-center">
                <div class="card border pt-4">
                    <i class="fab fa-microsoft fa-6x"></i>
                    <div class="card-body">
                        <div class="card-title">
                            Microsoft Office
                        </div>
                    </div>
                </div>
            </div>



        </div>

    </div>
</div>
