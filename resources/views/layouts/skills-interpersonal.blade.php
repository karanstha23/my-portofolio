<div class="my-interpersonalskills py-5">
    <div class="container pt-5">
        <div class="h1">Interpersonal Skills</div>
        <div class="bar"></div>
    </div>
    <div class="container my-interpersonalskills-container py-5">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <h1><img src="/images/skills.png" alt="" class="img-fluid" width="750px"></h1>
            </div>
            <div class="col-lg-6 col-md-12 col-12 mx-auto text-center">
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Excellent verbal and personal communication skills</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Flexible and Energetic</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Excellent interpersonal Skills</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Ability to work in team</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Well mannered</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Accuracy and attention to small details</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Ability to make qucik response</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Quick Learner</h4>
                <h4><i class="fas fa-check px-3" style="font-size: 15px"></i>  Able to manage stress in bad situations</h4>
            </div>
        </div>
    </div>
</div>
