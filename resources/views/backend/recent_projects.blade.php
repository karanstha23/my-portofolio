@extends('layouts.app')

@section('content')
<div class="container-fluid py-5">
    <div class="row justify-content-center">
        <div class=" col-lg-2 col-md-2 col-2">

        </div>
        <div class="col-lg-6 col-md-8 col-8">
            <h3 class="py-3"><strong>Project Information</strong></h3>

            <form method="POST" enctype="multipart/form-data" action="{{route('recent_projects.store')}}">
                @csrf
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="project_title" class="form-control @error('project_title') is-invalid @enderror" name="project_title" placeholder="Enter your project title" value="{{ old('project_title') }}">
                        @error('project_title')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                        <input type="text" onfocus="(this.type='file')" id="project_snapshot" class="form-control @error('project_snapshot') is-invalid @enderror" name="project_snapshot" placeholder="Project Snapshot" value="{{ old('project_snapshot') }}">
                        @error('project_snapshot')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                        <textarea class="form-control border-bottom @error('project_description') is-invalid @enderror" id="project_description" name="project_description" rows="3" placeholder="Project Description">{{old('project_description')}}</textarea>
                        @error('project_description')
                        <div class="alert"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group justify-content-center">
                    <div class="col-lg-12 col-md-12 col-12 text-center">
                      <button class="dashboard-button btn col-lg-4" type="submit">Add</button>
                    </div>

                </div>
                

            </form>
        </div>

        <div class="col-lg-4 col-md-3 col-3 py-3 text-center border-left">
            <h3 class="py-3"><strong>Education Information</strong></h3>
            @foreach($recentProjects ?? '' as $data)
            <div class="container-fluid">
                <div class="row">
                    <img src="/images/projects/{{$data->project_snapshot}}" alt="" class="img-fluid">
                </div>
            </div>
                
            @endforeach
        </div>
    </div>
</div>
@endsection
