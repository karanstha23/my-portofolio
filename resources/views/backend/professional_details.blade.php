@extends('layouts.app')

@section('content')
<div class="container-fluid py-5">
    <div class="row justify-content-center">
        <div class=" col-lg-2 col-md-2 col-2">

        </div>
        <div class="col-lg-5 col-md-5 col-5 dashboard_box">
            <h3 class="py-3"><strong>Professional Details</strong></h3>

            <form method="POST" enctype="multipart/form-data" action="{{route('professional_details.store')}}">
                @csrf
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="row form-group">
                    <div class="col">
                        <textarea class="form-control border-bottom @error('professional_summary') is-invalid @enderror" id="professional_summary" name="professional_summary" rows="4" placeholder="Professional Summary">{{old('professional_summary')}}</textarea>
                        @error('professional_summary')
                        <div class="alert"> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="professional_title" class="form-control @error('professional_title') is-invalid @enderror" name="professional_title" placeholder="Enter your professional title" value="{{ old('professional_title') }}">
                        @error('professional_title')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group justify-content-center">
                    <div class="col-lg-12 col-md-12 col-12 text-center">
                      <button class="dashboard-button btn col-lg-4" type="submit">Add</button>
                    </div>

                </div>
                

            </form>
        </div>

        <div class="col-lg-5 col-md-5 col-5 py-3">
            <h3 class="py-3"><strong>Skills</strong></h3>
            <form method="POST" enctype="multipart/form-data" action="{{route('skills.store')}}">
                @csrf
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="skill_name" class="form-control @error('skill_name') is-invalid @enderror" name="skill_name" placeholder="Enter your skill" value="{{ old('skill_name') }}">
                        @error('skill_name')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="skill_icon" class="form-control my icon-picker @error('skill_icon') is-invalid @enderror" name="skill_icon" placeholder="Enter your skill icon" value="{{ old('skill_icon') }}">
                        @error('skill_icon')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                        <select class="form-control @error('skill_category') is-invalid @enderror" id="skill_category" name="skill_category" >
                            <option selected value="">Choose a skill category</option>
                            <option value="0">Professional Skill</option>
                            <option value="1">Interpersonel Skill</option>
                            </select>
    
                            @error('skill_category')
                                <div class="alert"> {{$messaskill_categoryge}}</div>
                            @enderror
                    </div>

                </div>

                <div class="row form-group justify-content-center">
                    <div class="col-lg-12 col-md-12 col-12 text-center">
                      <button class="dashboard-button btn col-lg-4" type="submit">Add</button>
                    </div>

                </div>
                

            </form>
        </div>
    </div>
</div>
@endsection
