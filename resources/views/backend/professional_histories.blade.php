@extends('layouts.app')

@section('content')
<div class="container-fluid py-5">
    <div class="row justify-content-center">
        <div class=" col-lg-2 col-md-2 col-2">

        </div>
        <div class="col-lg-6 col-md-8 col-8">
            <h3 class="py-3"><strong>Work History</strong></h3>

            <form method="POST" action="{{route('professional_histories.store')}}">
                @csrf
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="job_title" class="form-control @error('job_title') is-invalid @enderror" name="job_title" placeholder="Enter your job title" value="{{ old('job_title') }}">

                      @error('job_title')
                        <div class="alert"> {{$message}}</div>
                      @enderror

                    </div>
                    <div class="col">
                        <select class="form-control @error('job_type') is-invalid @enderror" id="job_type" name="job_type" >
                        <option selected value="">Choose a job type</option>
                        <option value="Full Time">Full Time</option>
                        <option value="Part Time">Part Time</option>
                        <option value="Casual">Casual</option>
                        <option value="Contract">Contract</option>
                        <option value="Internship">Internship</option>
                        </select>

                        @error('job_type')
                            <div class="alert"> {{$message}}</div>
                        @enderror

                    </div>
                    
                    
                </div>

                <div class="row form-group">
                    <div class="col">
                        <input type="text" id="organisation_name" class="form-control @error('organisation_name') is-invalid @enderror" name="organisation_name" placeholder="Enter organization name" value={{old('organisation_name')}}>
                          @error('organisation_name')
                              <div class="alert"> {{$message}}</div>
                          @enderror
                      </div>
                      <div class="col">
                        <input type="text" id="organisation_location" class="form-control @error('organisation_location') is-invalid @enderror" name="organisation_location" placeholder="Enter organization location" value="{{ old('organisation_location') }}">
  
                          @error('organisation_location')
                              <div class="alert"> {{$message}}</div>
                          @enderror
  
                    </div>
                    
                    
                </div>

                <div class="form-group">
                    <textarea id="roles_responsibilities" class="form-control border-bottom @error('roles_responsibilities') is-invalid @enderror" name="roles_responsibilities" rows="3" placeholder="Enter your job roles and responsibilities">{{old('roles_responsibilities')}}</textarea>

                    @error('roles_responsibilities')
                        <div class="alert"> {{$message}}</div>
                    @enderror

                </div>

                <div class="row form-group">
                    <div class="col">
                        <label for="current_job"><strong>I am currently involved with this role.</strong>  </label>
                    </div>
                    <div class="col">
                        <input class="form-check-input" type="checkbox" id="current_job" name="current_job" value=1>
                    </div>

                </div>

                <div class="row form-group">
                    <div class="col">
                        <label for="current_job">Starting Date: </label>
                        <input type="date" id="starting_date" class="form-control @error('starting_date') is-invalid @enderror" name="starting_date" placeholder="Enter starting date" value="{{ old('starting_date') }}">

                        @error('starting_date')
                            <div class="alert"> {{$message}}</div>
                        @enderror

                    </div>
                    <div class="col" id="end_date">
                        <label for="current_job">End Date: </label>
                        <input type="date" id="ending_date" class="form-control @error('ending_date') is-invalid @enderror" name="ending_date" placeholder="Enter end date" value={{old('ending_date')}}>

                        @error('ending_date')
                            <div class="alert"> {{$message}}</div>
                        @enderror

                    </div>
                </div>

                <div class="row form-group py-3">
                    <div class="col">
                        <button class="btn dashboard-button col-lg-4" type="submit">Add Work</button>
                    </div>
                </div>

            </form>

        </div>

        <div class="col-lg-3 col-md-2 col-2 py-3 border-left">

            <h3 class="py-3"><strong>Work Experience</strong></h3>
            @foreach($professionalHistories_data ?? '' as $data)
            <div class="accordion" id="accordionExample{{$data->id}}">
                
                <div class="card border-white">
                  <div class="card-header" id="headingOne{{$data->id}}">
                    <h2 class="mb-0">
                      <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne{{$data->id}}" aria-expanded="true" aria-controls="collapseOne{{$data->id}}">
                        <div class="col-lg-12">
                            {{ $data->organisation_name}} AS
                        </div>
                        <div class="col-lg-12">
                            {{ $data->job_title}}
                        </div>
                      </button>
                    </h2>
                  </div>

                  <div id="collapseOne{{$data->id}}" class="collapse" aria-labelledby="headingOne{{$data->id}}" data-parent="#accordionExample{{$data->id}}">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 bg-danger my-auto">
                                <p class="my-4">3 Mon</p>

                            </div>
                            <div class="col-lg-9 col-md-12 col-12">
                                <p>{{ $data->starting_date }} - 
                                    @if($data->ending_date == NULL)
                                        CURRENT
                                    @else
                                        {{ $data->ending_date }}
                                    @endif
                                </p>
                            </div>
                            <div class="col-lg-12 col-md-12 col-12 mx-auto my-2">
                                <p>
                                    {{$data->roles_responsibilities}}
                                </p>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                

              </div>
              @endforeach
        </div>
</div>
@endsection
