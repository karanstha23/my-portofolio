@extends('layouts.app')

@section('content')
<div class="container-fluid py-5">
    <div class="row justify-content-center">
        <div class=" col-lg-2 col-md-2 col-2">

        </div>
        <div class="col-lg-6 col-md-8 col-8">
            <h3 class="py-3"><strong>Education Details</strong></h3>

            <form method="POST" enctype="multipart/form-data" action="{{route('educations.store')}}">
                @csrf
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif

                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="name_of_degree" class="form-control @error('name_of_degree') is-invalid @enderror" name="name_of_degree" placeholder="Enter your degree" value="{{ old('name_of_degree') }}">
                        @error('name_of_degree')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                    <div class="col">
                        <input type="text" class="form-control @error('educational_institution_name') is-invalid @enderror" name="educational_institution_name" placeholder="Enter educational institutional name" value={{old('educational_institution_name')}}>
                        @error('educational_institution_name')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                        <input type="text" onfocus="(this.type='date')" class="form-control @error('starting_date') is-invalid @enderror" name="starting_date" placeholder="Starting Date" value={{old('starting_date')}}>
                        @error('starting_date')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                    <div class="col">
                      <input type="text" onfocus="(this.type='date')" id="ending_date" class="form-control @error('ending_date') is-invalid @enderror" name="ending_date" placeholder="Ending Date" value="{{ old('ending_date') }}">
                        @error('ending_date')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                      <input type="file" id="institution_logo" class="form-control @error('institution_logo') is-invalid @enderror" name="institution_logo" placeholder="Institutional Logo" value="{{ old('institution_logo') }}">
                        @error('institution_logo')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group justify-content-center">
                    <div class="col-lg-12 col-md-12 col-12 text-center">
                      <button class="dashboard-button btn col-lg-4" type="submit">Add</button>
                    </div>

                </div>
                

            </form>
        </div>

        <div class="col-lg-4 col-md-3 col-3 py-3 text-center border-left">
            <h3 class="py-3"><strong>Education Information</strong></h3>
            @foreach($education_data ?? '' as $data)
            <div class="container-fluid">
                <div class="row py-3 mb-2 education_details">
                    <div class="col-lg-2 col-md-2 col-2">
                        <img src="/images/{{ $data->institution_logo }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-8 col-md-8 col-8">
                        <p class="strong"><strong>{{ $data->educational_institution_name }}</strong></p>
                        <p>{{ $data->name_of_degree }}</p>
                        <p>{{ $data->starting_date }} - {{ $data->ending_date }}</p>
                        

                    </div>
                    <div class="col-lg-1 col-md-1 col-1">
                        <i class="fab fa-laravel"></i>
                        <i class="fab fa-laravel"></i>
                    </div>
                </div>
            </div>
                
            @endforeach
        </div>
    </div>
</div>
@endsection
