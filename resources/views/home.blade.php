@extends('layouts.app')

@section('content')
<div class="container-fluid py-5">
    
    <div class="row justify-content-center">
        <div class=" col-lg-2 col-md-2 col-2">

        </div>
        <div class="col-lg-6 col-md-8 col-8">
            <h3 class="py-3"><strong>General Information</strong></h3>

            <form method="POST" action="general_information" id="general-information">

                @csrf
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                {{-- <div class="form-group">
                    <textarea class="form-control border-bottom @error('summary_info') is-invalid @enderror" id="summary" name="summary_info" rows="3" placeholder="Career Summary">{{old('summary_info')}}</textarea>
                    @error('summary_info')
                    <div class="alert"> {{$message}}</div>
                    @enderror
                </div> --}}

                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter your name" value="{{ old('name') }}">
                        @error('name')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                    <div class="col">
                      <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Enter your email" value={{old('email')}}>
                        @error('email')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                      <input type="text" id="address" class="form-control @error('address') is-invalid @enderror" name="address" placeholder="Enter your address" value="{{ old('address') }}">
                      @error('address')
                      <div class="alert "> {{$message}}</div>
                      @enderror
                    </div>
                    <div class="col">
                      <input type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" placeholder="Enter your contact number" value={{old('contact_number')}}>
                        @error('contact_number')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                      <input type="text" onfocus="(this.type='date')" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" placeholder="Enter your date of birth" value="{{old('date_of_birth')}}">
                        @error('date_of_birth')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                    <div class="col">
                      <input type="text" class="form-control @error('linkedin_link') is-invalid @enderror" name="linkedin_link" placeholder="Enter your linkedin url" value="{{old('linkedin_link')}}">
                        @error('linkedin_link')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col">
                      <input type="text" class="form-control @error('web_link') is-invalid @enderror" name="web_link" placeholder="Enter your web portofolio link" value={{old('web_link')}}>
                      @error('web_link')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div>
                    {{-- <div class="col">
                      <input type="text" class="form-control @error('country') is-invalid @enderror" name="country" placeholder="Enter residing country" value="{{old('country')}}">
                      @error('country')
                        <div class="alert "> {{$message}}</div>
                        @enderror
                    </div> --}}
                </div>

                <div class="row form-group justify-content-center">
                    <div class="col-lg-12 col-md-12 col-12 text-center">
                      <button class="dashboard-button btn col-lg-4" type="submit">Add</button>
                    </div>

                </div>



            </form>

                {{-- <div class="row">
                    <div class="col-lg-10 col-md-10 col-10 ">
                        <h3 class="py-3">Interests </h3>
                    </div>
                    <div class="col-lg-2 col-md-2 col-2 text-center d-flex justify-content-center">
                        <button class="btn my-4" id="add-interest">+</button>
                    </div>
                </div>
                <div class="container main-form">
                    <div class="row form-group" id="add-form-field">
                        <div class="col-lg-5">
                          <input type="text" class="form-control" name="icon[]" placeholder="Select an icon">
                        </div>
                        <div class="col-lg-5">
                          <input type="text" class="form-control" name="interest[]" placeholder="Enter your interest">
                        </div>
                    </div>
                </div> --}}




        </div>

        <div class="col-lg-3 col-md-2 col-2 py-3 text-center border-left">
            <h3><strong>Professional Skills</strong></h3>

            <form>
                @csrf
                <div class="alert alert-block" style="display: none;">

                    <button type="button" class="close" data-dismiss="alert">×</button>

                      <strong class="success-msg"></strong>

                </div>

                <div class="col py-2">
                    <input type="text" id="skills_icon" class=" form-control" name="skills_icon" placeholder="Select Favicon">
                    <span class="text-danger error-text skills_icon_err"></span>
                </div>
                <div class="col py-2">
                    <input type="text" id="skills" class="form-control" name="skills" placeholder="Enter Skills">
                    <span class="text-danger error-text skills_err"></span>
                </div>

                <div class="col py-2">
                    <button type="submit" class="btn dashboard-button col-lg-4" id="add_skills">Add Skill</button>
                </div>
            </form>

            <span class="badge badge-pill badge-dark px-2 my-4"><i class="fa fa-address-book" aria-hidden="true"></i> Laravel</span>



            <h3 class="py-3 border-top"><strong>Interpersonal Skills</strong></h3>

            <form method="POST" action="">
                @csrf
                <div class="col py-2">
                    <input type="text" class="form-control" name="interpersonal_skills" placeholder="Enter Interpersonal Skills">
                    <span class="text-danger error-text skills_err"></span>
                </div>

                <div class="col py-2">
                    <button type="submit" class="btn dashboard-button col-lg-8" id="add_skills">Add Interpersonal Skill</button>
                </div>
            </form>

        </div>
    </div>
</div>
@endsection
