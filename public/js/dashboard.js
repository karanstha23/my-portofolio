$(function () {
    $("#is_current").on("click", function () {
        if ($("#is_current").is(":checked")) {
            $("#end_date").hide();
        } else {
            $("#end_date").show();
        }
    });
});

$(function () {
    $("#add_work").on("click", function (e) {
        e.preventDefault();
        // alert('here');
        let _token = $("input[name = '_token']").val();
        let job_title = $("#job_title").val();
        let company_name = $("#company_name").val();
        let company_location = $("#company_location").val();
        let starting_date = $("#starting_date").val();
        let ending_date = $("#ending_date").val();
        let job_type = $("#job_type").val();
        let job_roles = $("#job_roles").val();
        let is_current = 1;

        if ($("#is_current").is(":checked")) {
             is_current = "1";
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
            type: "POST",
            uri: '{{ route("work_history.store") }}',
            data: {
                _token: _token,
                job_title: job_title,
                company_name: company_name,
                company_location: company_location,
                starting_date: starting_date,
                ending_date: ending_date,
                job_type: job_type,
                job_roles: job_roles,
                is_current: is_current,
            },

            success: function (response) {
                alert(response.success);
            },
            // error: function (response) {
            //     console.log(response);
                // if (response.responseJSON.errors.job_title) {
                //     $("#job_titleError").show();
                //     $("#job_titleError").text(
                //         response.responseJSON.errors.job_title
                //     );
                // } else {
                //     $("#job_titleError").hide();
                // }
                // if (response.responseJSON.errors.company_name) {
                //     $("#company_nameError").show();
                //     $("#company_nameError").text(
                //         response.responseJSON.errors.company_name
                //     );
                // } else {
                //     $("#company_nameError").hide();
                // }
                // if (response.responseJSON.errors.company_location) {
                //     $("#company_locationError").show();
                //     $("#company_locationError").text(
                //         response.responseJSON.errors.company_location
                //     );
                // } else {
                //     $("#company_locationError").hide();
                // }
                // if (response.responseJSON.errors.starting_date) {
                //     $("#starting_dateError").show();
                //     $("#starting_dateError").text(
                //         response.responseJSON.errors.starting_date
                //     );
                // } else {
                //     $("#starting_dateError").hide();
                // }
                // if (response.responseJSON.errors.ending_date) {
                //     $("#ending_dateError").show();
                //     $("#ending_dateError").text(
                //         response.responseJSON.errors.ending_date
                //     );
                // } else {
                //     $("#ending_dateError").hide();
                // }
                // if (response.responseJSON.errors.job_type) {
                //     $("#job_typeError").show();
                //     $("#job_typeError").text(
                //         response.responseJSON.errors.job_type
                //     );
                // } else {
                //     $("#job_typeError").hide();
                // }
                // if (response.responseJSON.errors.job_roles) {
                //     $("#job_rolesError").show();
                //     $("#job_rolesError").text(
                //         response.responseJSON.errors.job_roles
                //     );
                // } else {
                //     $("#job_rolesError").hide();
                // }
            // },
        });
    });
});
