<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GeneralInformationsController;
use App\Http\Controllers\EducationsController;
use App\Http\Controllers\ProfessionalSkillsController;
use App\Http\Controllers\ProfessionalHistoriesController;
use App\Http\Controllers\RecentProjectsController;
use App\Http\Controllers\ProfessionalDetailsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('general_information', GeneralInformationsController::class);
Route::resource('educations', EducationsController::class);
Route::resource('professional_histories', ProfessionalHistoriesController::class);
Route::resource('recent_projects', RecentProjectsController::class);
Route::resource('professional_details', ProfessionalDetailsController::class);



Route::resource('skills', ProfessionalSkillsController::class);


