<?php

namespace App\Http\Controllers;

use App\Models\ProfessionalHistories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfessionalHistoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $professionalHistories_data = ProfessionalHistories::All();

        // $working_period = 
        return view('backend/professional_histories')
            ->with('professionalHistories_data', $professionalHistories_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $isChecked = $request->has('current_job');
        // $value = $isChecked ? 1 : 0 ?? 0;
        // return($value);
        if($request->has('current_job'))
    {
        $validator = Validator::make($request->all(), [ 
            'job_title' => 'required',
            'organisation_name' => 'required',
            'organisation_location' => 'required',
            'current_job' => 'integer',
            'starting_date' => 'required',
            'roles_responsibilities' => 'required',
        ]);
    }
    else{
        $validator = Validator::make($request->all(), [ 
            'job_title' => 'required',
            'organisation_name' => 'required',
            'organisation_location' => 'required',
            'current_job' => 'integer',
            'starting_date' => 'required',
            'ending_date' => 'required',
            'roles_responsibilities' => 'required',
        ]);
    }
        if($validator->fails()){
            return redirect('/professional_histories/create')->withErrors($validator)->withInput();
        }
        else{
            $data = new ProfessionalHistories();
            ProfessionalHistories::create($request->all());
            return redirect('/professional_histories/create')->with('success','Worked Saved Sucessfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProfessionalHistories  $professionalHistories
     * @return \Illuminate\Http\Response
     */
    public function show(ProfessionalHistories $professionalHistories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProfessionalHistories  $professionalHistories
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfessionalHistories $professionalHistories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProfessionalHistories  $professionalHistories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfessionalHistories $professionalHistories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProfessionalHistories  $professionalHistories
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfessionalHistories $professionalHistories)
    {
        //
    }
}
