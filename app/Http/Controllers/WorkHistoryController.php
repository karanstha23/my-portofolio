<?php

namespace App\Http\Controllers;

use App\Models\WorkHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WorkHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("backend.work_history");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = $request->input('is_current');
        return($check);

      $request->validate([
            'job_title' => 'required',
            'company_name' => 'required',
            'company_location' => 'required',
            'is_current' => 'required|integer',
            'starting_date' => 'required',
            'ending_date' => 'required_if:is_current, ==, 0|nullable',
            'job_type' => 'required',
            'job_roles' => 'required',
        ]);
        if($validator->fails()){
            return redirect('/work_history')->withErrors($validator)->withInput();
        }
        else{
            WorkHistory::create($request->all());
            return redirect('/work_history')->with('success','Worked Saved Sucessfully');
        }
        }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkHistory  $workHistory
     * @return \Illuminate\Http\Response
     */
    public function show(WorkHistory $workHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkHistory  $workHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkHistory $workHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkHistory  $workHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkHistory $workHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkHistory  $workHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkHistory $workHistory)
    {
        //
    }
}
