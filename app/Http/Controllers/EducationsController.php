<?php

namespace App\Http\Controllers;

use App\Models\educations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class EducationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $education_data = Educations::All();
        return view("backend/educations/create")->with('education_data', $education_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $education_data = Educations::All();
        return view("backend/educations")->with('education_data', $education_data);
        // return view("backend/educations");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name_of_degree' => 'required',
            'starting_date' => 'date|required',
            // 'ending_date' => 'required',
            'educational_institution_name' => 'required',
            'institution_logo' => 'required',

        ]);

        if($validator->fails()){
            return redirect('/educations/create')->withErrors($validator)->withInput();
        }
        else{
            $data = new Educations();
            // Educations::create($request->all());
  
            $date = Carbon::now();

            if($request->has('institution_logo')){
                
                $file = $request->file('institution_logo');
                $name = $file->hashName();
                $imageName = $date.'.'.$name;
                $data->institution_logo = $imageName;
     
                
            }
            $data->name_of_degree = $request->get('name_of_degree');
            $data->starting_date = $request->get('starting_date');
            $data->ending_date = $request->get('ending_date');
            $data->educational_institution_name = $request->get('educational_institution_name');
            $request->file("institution_logo")->move(public_path('images'), $imageName);
            $data->save();

            return redirect('/educations/create')->with('success','Educations Informations Saved Sucessfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\educations  $educations
     * @return \Illuminate\Http\Response
     */
    public function show(educations $educations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\educations  $educations
     * @return \Illuminate\Http\Response
     */
    public function edit(educations $educations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\educations  $educations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, educations $educations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\educations  $educations
     * @return \Illuminate\Http\Response
     */
    public function destroy(educations $educations)
    {
        //
    }
}
