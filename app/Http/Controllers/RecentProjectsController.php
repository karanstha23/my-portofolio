<?php

namespace App\Http\Controllers;

use App\Models\RecentProjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class RecentProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $recentProjects = RecentProjects::All();
        return view("/backend/recent_projects")->with('recentProjects', $recentProjects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'project_title' => 'required',
            'project_description' => 'required',
            'project_snapshot' => 'required|file'
        ]);
        
        if($validation->fails()){
            return redirect()->back()->withErrors($validation)->withInput();
        }
        else{
            $recentProjects = new RecentProjects();
            $recentProjects->project_title = $request->get('project_title');
            $recentProjects->project_description = $request->get('project_description');

            $date = Carbon::now();
            $file = $request->file('project_snapshot');
            $image = $file->hashName();
            $imageName = $date.'.'.$image;
            $recentProjects->project_snapshot = $imageName;

            $request->file('project_snapshot')->move(public_path('images/projects'), $imageName);

            $recentProjects->save();
            return redirect()->back()->withSuccess('Project Added Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RecentProjects  $recentProjects
     * @return \Illuminate\Http\Response
     */
    public function show(RecentProjects $recentProjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RecentProjects  $recentProjects
     * @return \Illuminate\Http\Response
     */
    public function edit(RecentProjects $recentProjects)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RecentProjects  $recentProjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecentProjects $recentProjects)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RecentProjects  $recentProjects
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecentProjects $recentProjects)
    {
        //
    }
}
