<?php

namespace App\Http\Controllers;

use App\Models\ProfessionalDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfessionalDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend/professional_details');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'professional_summary' => 'required',
            'professional_title' => 'required',
        ]);

        if($validation->fails()){
            return redirect()->back()->withInput()->withErrors($validation);
        }
        else{
            $professionalDetails = new ProfessionalDetails();
            ProfessionalDetails::create($request->all());
            return redirect()->back()->with('success', 'Professional Details Added');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProfessionalDetails  $professionalDetails
     * @return \Illuminate\Http\Response
     */
    public function show(ProfessionalDetails $professionalDetails)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProfessionalDetails  $professionalDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfessionalDetails $professionalDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProfessionalDetails  $professionalDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfessionalDetails $professionalDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProfessionalDetails  $professionalDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfessionalDetails $professionalDetails)
    {
        //
    }
}
