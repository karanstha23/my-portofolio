<?php

namespace App\Http\Controllers;

use App\Models\GeneralInformations;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GeneralInformationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email|required',
            'contact_number' => 'required',
            'address' => 'required',
            'date_of_birth' => 'date|required',
            // 'linkedin_link' => 'required',
            // 'web_link' => 'date|required',

        ]);

        if($validator->fails()){
            return redirect('/home')->withErrors($validator)->withInput();
        }
        else{
            $data = new GeneralInformations();
            GeneralInformations::create($request->all());
            return redirect('/home')->with('success','General Information Saved Sucessfully');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GeneralInformations  $generalInformations
     * @return \Illuminate\Http\Response
     */
    public function show(GeneralInformations $generalInformations)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\GeneralInformations  $generalInformations
     * @return \Illuminate\Http\Response
     */
    public function edit(GeneralInformations $generalInformations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GeneralInformations  $generalInformations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GeneralInformations $generalInformations)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GeneralInformations  $generalInformations
     * @return \Illuminate\Http\Response
     */
    public function destroy(GeneralInformations $generalInformations)
    {
        //
    }
}
