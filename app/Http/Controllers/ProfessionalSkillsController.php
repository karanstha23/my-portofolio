<?php

namespace App\Http\Controllers;

use App\Models\ProfessionalSkills;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfessionalSkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return "here";
        $validator = Validator::make($request->all(), [
            'skills_icon' => 'required',
            'skills' => 'required',
        ]);
        // return('here');

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()]);
        }
        else{
            ProfessionalSkills::create($request->all());
            return response()->json(['success'=>'New Skills Added']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProfessionalSkills  $professionalSkills
     * @return \Illuminate\Http\Response
     */
    public function show(ProfessionalSkills $professionalSkills)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProfessionalSkills  $professionalSkills
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfessionalSkills $professionalSkills)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProfessionalSkills  $professionalSkills
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfessionalSkills $professionalSkills)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProfessionalSkills  $professionalSkills
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfessionalSkills $professionalSkills)
    {
        //
    }
}
