<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Educations extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['name_of_degree', 'starting_date', 'ending_date', 'educational_institution_name', 'institution_logo'];

    protected $dates = ['deleated_at'];

}
