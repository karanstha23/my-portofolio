<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecentProjects extends Model
{
    use HasFactory;
    use SoftDeletes;
    public $fillable = ['project_title', 'project_description', 'project_snapshot'];

    protected $dates = ['deleated_at'];


}
