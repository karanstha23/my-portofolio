<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkHistory extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['job_title', 'company_name', 'company_location', 'starting_date', 'ending_date', 'job_type', 'job_roles'];
    private $protected = ["deleted_at"];
}
