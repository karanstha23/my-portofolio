<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfessionalDetails extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['professional_summary', 'professional_title'];
    protected $dates = ['deleated_at'];
}
