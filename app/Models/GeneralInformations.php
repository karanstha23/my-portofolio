<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralInformations extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['name', 'email', 'contact_number', 'address', 'date_of_birth', 'linkedin_link', 'web_link'];

    protected $dates = ['deleated_at'];
}
