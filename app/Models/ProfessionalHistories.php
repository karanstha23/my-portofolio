<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfessionalHistories extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['job_title', 'organisation_name', 'organisation_location', 'starting_date', 'ending_date', 'current_job', 'roles_responsibilities'];

    protected $dates = ['deleated_at'];
}
