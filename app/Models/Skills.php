<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skills extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = ['skill_title', 'skill_icon', 'skill_category'];

    protected $dates = ['deleated_at'];

}
